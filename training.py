import tensorflow as tf
import cv2
import numpy as np
import os
import time
import random
from bisenet import Bisenet

BATCH_SIZE = 16
IMG_WIDTH = 256
IMG_HEIGHT = 256
PATH = "/home/nhatdeptrai/Desktop/cds-segmentation/Data_final_1_hjhj/"
NUM_CLASSES = 5

LABEL_COLOR = np.array([[0,0,0],[142,0,0], [128,64,128], [0,220,220], [180,130,70]], dtype=np.float32)

def apply_brightness_contrast(input_img, brightness=0, contrast=0):
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow) / 255
        gamma_b = shadow

        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131 * (contrast + 127) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127 * (1 - f)

        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf


def get_random_crop(image, crop_height, crop_width):
    max_x = image.shape[1] - crop_width
    max_y = image.shape[0] - crop_height

    x = np.random.randint(0, max_x + 1)
    y = np.random.randint(0, max_y + 1)
    # y = max(0, y)

    crop = image[y: y + crop_height, x: x + crop_width]

    return crop


def padding_resize(im, desired_size=128, isLabel=False):
    old_size = im.shape[:2]  # old_size is in (height, width) format
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) formatNN
    if isLabel:
        im = cv2.resize(im, (new_size[1], new_size[0]), interpolation=cv2.INTER_NEAREST)
    else:
        im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


def one_hot_tensors_to_color_images(output_id_classes):
    output_batch = tf.reshape(tf.matmul(
        tf.reshape(tf.one_hot(tf.argmax(output_id_classes, -1), NUM_CLASSES), [-1, NUM_CLASSES]), LABEL_COLOR),
        [-1, IMG_HEIGHT, IMG_WIDTH, 3])
    return output_batch


# cce = Categorical Cross Entropy
def loss_cce(label, predict):
    pos_weight = tf.constant([1., 2., 1., 5., 4.])
    return tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(
       tf.cast(label, tf.float32), predict, pos_weight, name=None
    ))

    # return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf.cast(label, tf.float32), logits=predict))


def l2(label, predict):
    return tf.reduce_mean((tf.cast(label, dtype=tf.float32) - predict) ** 2)


def dice_loss(label, predict):
    label = tf.cast(label, dtype=tf.float32)
    numerator = 2 * tf.reduce_sum(label[:, :, :, :] * predict[:, :, :, :], axis=(1, 2, 3))
    denominator = tf.reduce_sum(label[:, :, :, :] + predict[:, :, :, :], axis=(1, 2, 3))

    return tf.reduce_mean(1 - numerator / denominator)


from tensorflow.python.ops import array_ops
def focal_loss(target_tensor, prediction_tensor, weights=None, alpha=0.25, gamma=2):
    r"""Compute focal loss for predictions.
        Multi-labels Focal loss formula:
            FL = -alpha * (z-p)^gamma * log(p) -(1-alpha) * p^gamma * log(1-p)
                 ,which alpha = 0.25, gamma = 2, p = sigmoid(x), z = target_tensor.
    Args:
     prediction_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing the predicted logits for each class
     target_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing one-hot encoded classification targets
     weights: A float tensor of shape [batch_size, num_anchors]
     alpha: A scalar tensor for focal loss alpha hyper-parameter
     gamma: A scalar tensor for focal loss gamma hyper-parameter
    Returns:
        loss: A (scalar) tensor representing the value of the loss function
    """
    target_tensor = tf.cast(target_tensor, tf.float32)
    sigmoid_p = tf.nn.sigmoid(prediction_tensor)
    zeros = array_ops.zeros_like(sigmoid_p, dtype=sigmoid_p.dtype)

    # For poitive prediction, only need consider front part loss, back part is 0;
    # target_tensor > zeros <=> z=1, so poitive coefficient = z - p.
    pos_p_sub = array_ops.where(target_tensor > zeros, target_tensor - sigmoid_p, zeros)

    # For negative prediction, only need consider back part loss, front part is 0;
    # target_tensor > zeros <=> z=1, so negative coefficient = 0.
    neg_p_sub = array_ops.where(target_tensor > zeros, zeros, sigmoid_p)
    per_entry_cross_ent = - alpha * (pos_p_sub ** gamma) * tf.log(tf.clip_by_value(sigmoid_p, 1e-8, 1.0)) \
                          - (1 - alpha) * (neg_p_sub ** gamma) * tf.log(tf.clip_by_value(1.0 - sigmoid_p, 1e-8, 1.0))
    return tf.reduce_sum(per_entry_cross_ent)

def one_hot(label_gray):
    label_channel_list = []
    for class_id in range(5):
        equal_map = tf.equal(label_gray, class_id)
        binary_map = tf.to_int32(equal_map)
        label_channel_list.append(binary_map)
    label_xin = tf.stack(label_channel_list, axis=3)
    label_xin = tf.squeeze(label_xin, axis=-1)
    return label_xin


tf.reset_default_graph()

# tbc = TensorBoardColab(graph_path="/content/log/")

list_label = [x for x in os.listdir(PATH + 'labels/') if ".png" in x]

input_tensor = tf.placeholder(dtype=tf.float32, shape=(BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, 3))
label_gray_tensor = tf.placeholder(dtype=tf.float32, shape=(BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, 1))
label_tensor = one_hot(label_gray_tensor)

predict_tensor, predict_sup1, predict_sup2 = Bisenet(input_tensor, NUM_CLASSES, mode="training")

predict_image = one_hot_tensors_to_color_images(predict_tensor)
label_image = one_hot_tensors_to_color_images(label_tensor)

input_summary = tf.summary.image("Image", input_tensor, max_outputs=2)
label_summary = tf.summary.image("Label", label_image, max_outputs=2)
predict_summary = tf.summary.image("Predict", predict_image, max_outputs=2)
image_summary = tf.summary.merge([input_summary, predict_summary, label_summary])

saver = tf.train.Saver()

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

# Caculate loss
cce_ = loss_cce(label=label_tensor, predict=predict_tensor)
cce1 = loss_cce(label=label_tensor, predict=predict_sup1)
cce2 = loss_cce(label=label_tensor, predict=predict_sup2)
dice_ = dice_loss(label=label_tensor, predict=predict_tensor)
dice1 = dice_loss(label=label_tensor, predict=predict_sup1)
dice2 = dice_loss(label=label_tensor, predict=predict_sup2)

focal = focal_loss(label_tensor, predict_tensor)
cce = cce_ + cce1 + cce2
dice = dice_

loss = cce + dice + focal

total_parameters = 0
#iterating over all variables
for variable in tf.trainable_variables():
    local_parameters=1
    shape = variable.get_shape()  #getting shape of a variable
    for i in shape:
        local_parameters*=i.value  #mutiplying dimension values
    total_parameters+=local_parameters
print("Total params: ", total_parameters)

optimizer = tf.train.RMSPropOptimizer(0.001, 0.9, 0.9).minimize(loss, var_list=tf.trainable_variables())
mean_IOU, mean_IOU_update = tf.contrib.metrics.streaming_mean_iou(labels=tf.argmax(label_tensor, axis=-1),
                                                                  predictions=tf.argmax(predict_tensor, axis=-1),
                                                                  num_classes=NUM_CLASSES)

with tf.control_dependencies([mean_IOU_update]):
    mean_IOU_scalar = tf.summary.scalar("Mean_IoU", mean_IOU, family="Metric")
cce_scalar = tf.summary.scalar("CCE_SUM", cce, family="Loss")
focal_scalar = tf.summary.scalar("FOCAL", focal, family="Loss")
dice_scalar = tf.summary.scalar("DICE_SUM", dice, family="Loss")
loss_scalar = tf.summary.scalar("TOTAL_LOSS", loss, family="Loss")
scalar_summary = tf.summary.merge([cce_scalar, dice_scalar, focal_scalar, loss_scalar, mean_IOU_scalar])

# loss = tf.reduce_sum(tf.square(result - y))
# optimizer = tf.train.GradientDescentOptimizer(0.0001)
# train = optimizer.minimize(loss)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
sess.run(tf.global_variables_initializer())
sess.run(tf.local_variables_initializer())

saver.restore(sess, "./model/model.ckpt")

writer = tf.summary.FileWriter("./log", sess.graph)
# tf.summary.FileWriter("log", sess.graph)

dem_epoch = 0
for epoch in range(10000):
    random.shuffle(list_label)
    dem_step = 0
    for i in range(0, len(list_label) + 1, BATCH_SIZE):
        batch_labels = []
        batch_images = []
        if i + BATCH_SIZE >= len(list_label):
            break
        for j in range(BATCH_SIZE):
            # try:
                label = cv2.imread(PATH + "labels/" + list_label[i + j])
                image = cv2.imread(PATH + "RGBs/" + list_label[i + j])

                # Random distort
                if random.randint(1, 10) >= 5:
                    brightness = random.randint(-10, 10)
                    contrast = random.randint(-10, 10)
                    image = apply_brightness_contrast(image, brightness, contrast)

                # #Random crop
                # if random.randint(1,10) >= 5:
                #     img_and_label = np.concatenate([image, label], axis=-1)
                #     rate = random.randint(1,3)
                #     crop_result = get_random_crop(img_and_label, image.shape[0]//rate, image.shape[1]//rate)
                #     image = crop_result[:, :, :3]
                #     label = crop_result[:, :, 3:]

                image = cv2.resize(image, (IMG_HEIGHT, IMG_WIDTH))
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                label = cv2.resize(label, (IMG_HEIGHT, IMG_WIDTH), interpolation=cv2.INTER_NEAREST)

                label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
                label = np.expand_dims(label, axis=0)
                label = np.expand_dims(label, axis=-1)
                image = image / 255.
                image = np.expand_dims(image, axis=0)

                batch_labels.append(label)
                batch_images.append(image)

            # except:
            #     print(list_label[i + j])

        batch_labels = np.concatenate(batch_labels, axis=0)
        batch_images = np.concatenate(batch_images, axis=0)

        t = time.time()

        _, loss_val, mean_IOU_val, image_summary_val, scalar_summary_val = sess.run(
            [optimizer, loss, mean_IOU, image_summary, scalar_summary],
            feed_dict={input_tensor: batch_images, label_gray_tensor: batch_labels})
        # predict_val = sess.run(predict_image, feed_dict={input_tensor:batch_images, label_gray_tensor:batch_labels})

        print(
            "Epoch: {: >4}".format(str(dem_epoch)) + ", Step: {: >3}".format(str(dem_step)) + ", Loss: {: >15}".format(
                str(loss_val)) + ", MeanIOU: {: >10}".format(str(mean_IOU_val)) + "| Time: {: >20}".format(
                str(time.time() - t)))

        # print(time.time()-t)

        if (dem_epoch * int(491 / BATCH_SIZE) + dem_step) % 20 == 0:
            writer.add_summary(image_summary_val, global_step=dem_epoch * 240 + dem_step)

        writer.add_summary(scalar_summary_val, global_step=dem_epoch * 1220 / BATCH_SIZE + dem_step)

        dem_step = dem_step + 1
    dem_epoch = dem_epoch + 1
    saver.save(sess, "./model/model.ckpt")
