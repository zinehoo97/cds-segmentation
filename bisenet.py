import tensorflow as tf
from tensorflow.contrib import slim


def Upsampling(inputs, scale, name=None):
    # return tf.image.resize_nearest_neighbor(inputs, size=[tf.shape(inputs)[1] * scale, tf.shape(inputs)[2] * scale])
    return tf.image.resize_bilinear(inputs, size=[tf.shape(inputs)[1] * scale, tf.shape(inputs)[2] * scale], name=name)


def ConvBlock(inputs, n_filters, kernel_size=[3, 3], strides=1):
    """
    Basic conv block for Encoder-Decoder
    Apply successivly Convolution, BatchNormalization, ReLU nonlinearity
    """
    net = slim.conv2d(inputs, n_filters, kernel_size, stride=[strides, strides], activation_fn=None)
    net = tf.nn.relu(slim.batch_norm(net, fused=True))
    return net


def AttentionRefinementModule(inputs, n_filters):
    inputs = slim.conv2d(inputs, n_filters, [3, 3], activation_fn=None)
    inputs = tf.nn.relu(slim.batch_norm(inputs, fused=True))

    # Global average pooling
    net = tf.reduce_mean(inputs, [1, 2], keep_dims=True)

    net = slim.conv2d(net, n_filters, kernel_size=[1, 1])
    net = slim.batch_norm(net, fused=True)
    net = tf.sigmoid(net)

    net = tf.multiply(inputs, net)

    return net


def FeatureFusionModule(input_1, input_2, n_filters):
    inputs = tf.concat([input_1, input_2], axis=-1)
    inputs = ConvBlock(inputs, n_filters=n_filters, kernel_size=[3, 3])

    # Global average pooling
    net = tf.reduce_mean(inputs, [1, 2], keep_dims=True)

    net = slim.conv2d(net, n_filters, kernel_size=[1, 1])
    net = tf.nn.relu(net)
    net = slim.conv2d(net, n_filters, kernel_size=[1, 1])
    net = tf.sigmoid(net)

    net = tf.multiply(inputs, net)

    net = tf.add(inputs, net)

    return net


def block(input, mid_out_channels, has_proj, stride, dilation=1, expansion=4):
    if has_proj:
        shortcut = slim.separable_conv2d(input, mid_out_channels * expansion, [3, 3], stride=stride)
        shortcut = slim.batch_norm(shortcut)
    else:
        shortcut = input
    residual = slim.separable_conv2d(input, mid_out_channels, [3, 3], stride=stride, rate=dilation)
    residual = slim.batch_norm(residual)
    residual = tf.nn.relu(residual)
    # residual = slim.separable_conv2d(residual, mid_out_channels, [3, 3])
    # residual = slim.batch_norm(residual)
    # residual = tf.nn.relu(residual)
    residual = slim.separable_conv2d(residual, mid_out_channels * expansion, [3, 3])
    residual = slim.batch_norm(residual)
    output = tf.nn.relu(residual + shortcut)
    return output


@slim.add_arg_scope
def make_layers(input, layers, channel, stride, scope, outputs_collections=None):
    with tf.variable_scope(scope, 'stage', [input]) as sc:
        has_proj = True if stride > 1 else False
        with tf.variable_scope('block1'):
            net = block(input, channel, has_proj, stride)
        for i in range(1, layers):
            with tf.variable_scope('block' + str(i + 1)):
                net = block(net, channel, False, stride=1)
        return slim.utils.collect_named_outputs(outputs_collections, sc.name, net)


def xception_arg_scope(weight_decay=0.00001,
                       batch_norm_decay=0.9,
                       batch_norm_epsilon=1e-5):
    '''
    The arg scope for xception model. The weight decay is 1e-5 as seen in the paper.
    INPUTS:
    - weight_decay(float): the weight decay for weights variables in conv2d and separable conv2d
    - batch_norm_decay(float): decay for the moving average of batch_norm momentums.
    - batch_norm_epsilon(float): small float added to variance to avoid dividing by zero.
    OUTPUTS:
    - scope(arg_scope): a tf-slim arg_scope with the parameters needed for xception.
    '''
    # Set weight_decay for weights in conv2d and separable_conv2d layers.
    with slim.arg_scope([slim.conv2d, slim.separable_conv2d],
                        weights_regularizer=slim.l2_regularizer(weight_decay),
                        biases_initializer=None,
                        activation_fn=None):
        # Set parameters for batch_norm. Note: Do not set activation function as it's preset to None already.
        with slim.arg_scope([slim.batch_norm],
                            decay=batch_norm_decay,
                            epsilon=batch_norm_epsilon) as scope:
            return scope


def Xception39(inputs, is_training=True, reuse=None, scope='Xception39'):
    with slim.arg_scope(xception_arg_scope()):
        layers = [4, 8, 4]
        channels = [16, 32, 64]
        with tf.variable_scope(scope, 'xception', [inputs], reuse=reuse) as sc:
            end_points_collection = sc.name + '_end_points'

            with slim.arg_scope([slim.separable_conv2d], depth_multiplier=1), \
                 slim.arg_scope([make_layers], outputs_collections=end_points_collection), \
                 slim.arg_scope([slim.batch_norm], is_training=is_training):
                # ===========ENTRY FLOW==============
                net = slim.conv2d(inputs, 8, [3, 3], stride=2, padding='same', scope='pool1')
                net = slim.batch_norm(net, scope='pool1_bn1')
                net = tf.nn.relu(net, name='pool1_relu1')
                net = slim.max_pool2d(net, [3, 3], stride=2, padding='same', scope='pool2')

                # =========== STAGE ==============
                for i in range(len(layers)):
                    net = make_layers(net, layers[i], channels[i], stride=2, scope='stage' + str(i + 1))

                end_points = slim.utils.convert_collection_to_dict(end_points_collection)
                end_points['pool4'] = end_points[scope + '/stage2']
                end_points['pool5'] = net

            return net, end_points


def Bisenet(input_tensor, NUM_CLASSES, mode="inference"):
    batch_norm_params = {"scale": True,
                         # Decay for the moving averages.
                         "decay": 0.9,
                         # Epsilon to prevent 0s in variance.
                         "epsilon": 1e-5,
                         'updates_collections': tf.GraphKeys.UPDATE_OPS,  # Ensure that updates are done within a frame
                         }
    initializer = slim.xavier_initializer()
    with tf.variable_scope('spatial_net'):
        with slim.arg_scope([slim.conv2d], biases_initializer=None, weights_initializer=initializer):
            with slim.arg_scope([slim.batch_norm], **batch_norm_params):
                # spatial_net = ConvBlock(input_tensor, n_filters=64, kernel_size=[3, 3], strides=2)
                # spatial_net = ConvBlock(spatial_net, n_filters=64, kernel_size=[1, 1], strides=2)

                spatial_net = ConvBlock(input_tensor, n_filters=64, kernel_size=[5, 5], strides=2)
                spatial_net = ConvBlock(spatial_net, n_filters=64, kernel_size=[3, 3], strides=2)

                spatial_net = ConvBlock(spatial_net, n_filters=64, kernel_size=[3, 3], strides=2)
                spatial_net = ConvBlock(spatial_net, n_filters=128, kernel_size=[1, 1])

    ### Context path
    logits, end_points = Xception39(input_tensor)

    ### Combining the paths
    with tf.variable_scope('combine_path', reuse=tf.AUTO_REUSE):
        with slim.arg_scope([slim.conv2d], biases_initializer=None, weights_initializer=initializer):
            with slim.arg_scope([slim.batch_norm], **batch_norm_params):
                # tail part
                size = tf.shape(end_points['pool5'])[1:3]
                global_context = tf.reduce_mean(end_points['pool5'], [1, 2], keep_dims=True)
                global_context = slim.conv2d(global_context, 128, 1, [1, 1], activation_fn=None)
                global_context = tf.nn.relu(slim.batch_norm(global_context, fused=True))
                global_context = tf.image.resize_bilinear(global_context, size=size)

                net_5 = AttentionRefinementModule(end_points['pool5'], n_filters=128)
                net_4 = AttentionRefinementModule(end_points['pool4'], n_filters=128)

                net_5 = tf.add(net_5, global_context)
                net_5 = Upsampling(net_5, scale=2)
                net_5 = ConvBlock(net_5, n_filters=128, kernel_size=[3, 3])
                net_4 = tf.add(net_4, net_5)
                net_4 = Upsampling(net_4, scale=2)
                net_4 = ConvBlock(net_4, n_filters=128, kernel_size=[3, 3])

                context_net = net_4

                net = FeatureFusionModule(input_1=spatial_net, input_2=context_net, n_filters=256)
                net = ConvBlock(net, n_filters=64, kernel_size=[3, 3])

                # # Upsampling
                # net = Upsampling(net, scale=2)
                # net = slim.conv2d(net, 32, [3, 3], rate=2, activation_fn=tf.nn.relu, biases_initializer=None,
                #                   normalizer_fn=slim.batch_norm)
                #
                # net = slim.conv2d(net, NUM_CLASSES, [1, 1], activation_fn=None, scope='logits')
                # net = Upsampling(net, 4, name="output")

                net = slim.conv2d(net, NUM_CLASSES, [1, 1], activation_fn=None, scope='logits')
                net = Upsampling(net, scale=8, name="output")

                if mode == "training":
                    sup1 = slim.conv2d(net_5, NUM_CLASSES, [1, 1], activation_fn=None, scope='supl1')
                    sup2 = slim.conv2d(net_4, NUM_CLASSES, [1, 1], activation_fn=None, scope='supl2')
                    sup1 = Upsampling(sup1, scale=16)
                    sup2 = Upsampling(sup2, scale=8)

                    return net, sup1, sup2
                return net