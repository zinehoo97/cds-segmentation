import os
import tensorflow as tf
import config as cfg


def one_hot_it(label):
  semantic_map = []
  for colour in range(cfg.NUM_CLASSES):
    # colour_map = np.full((label.shape[0], label.shape[1], label.shape[2]), colour, dtype=int)
    equality = tf.equal(label, colour)
    class_map = tf.reduce_all(equality, axis=-1)
    semantic_map.append(class_map)
  semantic_map = tf.stack(semantic_map, axis=-1)

  return semantic_map


def _distort_color(image, label, color_ordering=0, fast_mode=True, scope=None):
  with tf.name_scope(scope, 'distort_color', [image]):
    if fast_mode:
      if color_ordering == 0:
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
      else:
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
    else:
      if color_ordering == 0:
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
        image = tf.image.random_hue(image, max_delta=0.2)
        image = tf.image.random_contrast(image, lower=0.5, upper=1.5)
      elif color_ordering == 1:
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
        image = tf.image.random_contrast(image, lower=0.5, upper=1.5)
        image = tf.image.random_hue(image, max_delta=0.2)
      elif color_ordering == 2:
        image = tf.image.random_contrast(image, lower=0.5, upper=1.5)
        image = tf.image.random_hue(image, max_delta=0.2)
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
      elif color_ordering == 3:
        image = tf.image.random_hue(image, max_delta=0.2)
        image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
        image = tf.image.random_contrast(image, lower=0.5, upper=1.5)
        image = tf.image.random_brightness(image, max_delta=32. / 255.)
      else:
        raise ValueError('color_ordering must be in [0, 3]')

    # The random_* ops do not necessarily clamp.
    return tf.clip_by_value(image, 0.0, 1.0), label


def prepare_label(image, label):
    label = one_hot_it(label)
    label = tf.to_int32(label)
    return image, label

def _parse_function(image_filename, label_filename):
    img_contents = tf.read_file(image_filename)
    label_contents = tf.read_file(label_filename)

    img = tf.image.decode_png(img_contents, channels=3)
    img = tf.image.convert_image_dtype(img, dtype=tf.float32)
    img = img / 255.

    label = tf.image.decode_png(label_contents, channels=1)
    # label = tf.argmax(label, axis=-1, output_type=tf.int32)
    # label = tf.expand_dims(label, axis=-1)

    return img, label


def _check_size(image, label):
    image = tf.image.resize_image_with_pad(image, cfg.IMAGE_SIZE, cfg.IMAGE_SIZE, method=tf.image.ResizeMethod.BILINEAR)
    label = tf.image.resize_image_with_pad(label, cfg.IMAGE_SIZE, cfg.IMAGE_SIZE,
                                          method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    return image, label


class DataLoader(object):
    def __init__(self):
        self.dataset = None
        self.iterator = None
        self.build()

    def build(self):
        self.prepare_data()
        self.build_iterator()

    def prepare_data(self):
        batch_size = cfg.BATCH_SIZE

        input_names = []
        output_names = []

        file_names = os.listdir(cfg.LABEL_DIR)
        for file in file_names:
            input_names.append(cfg.INPUT_DIR + "/" + file)
            output_names.append(cfg.LABEL_DIR + "/" + file)

        # input_names.sort(), output_names.sort()

        dataset = tf.data.Dataset.from_tensor_slices((input_names, output_names))
        dataset = dataset.map(lambda x, y: _parse_function(x, y), num_parallel_calls=cfg.THREADS)
        dataset = dataset.map(lambda image, label: _check_size(image, label), num_parallel_calls=cfg.THREADS)
        # dataset = dataset.map(lambda image, label: _distort_color(image, label, fast_mode=True))

        # dataset = dataset.map(lambda image, label: _check_size(image, label))
        dataset = dataset.map(lambda image, label: prepare_label(image, label), num_parallel_calls=cfg.THREADS)
        # dataset = dataset.shuffle(buffer_size=200)
        dataset = dataset.batch(batch_size)
        dataset = dataset.repeat()
        self.dataset = dataset

    def build_iterator(self):
        self.iterator = self.dataset.make_one_shot_iterator()

    def get_one_batch(self):
        return self.iterator.get_next()