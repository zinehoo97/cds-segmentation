import tensorflow as tf
import cv2
import numpy as np
import os
import time
import random
from bisenet import Bisenet

IMG_WIDTH = 256
IMG_HEIGHT = 256

IMAGE_PATH = "/home/nhatdeptrai/Desktop/cds-segmentation/test/images/"
PREDICT_PATH = "/home/nhatdeptrai/Desktop/cds-segmentation/test/predict/"

NUM_CLASSES = 5

LABEL_COLOR = np.array([[0,0,0],[142,0,0], [128,64,128], [0,220,220], [180,130,70]], dtype=np.float32)

def one_hot_tensors_to_color_images(output_id_classes):
    output_batch = tf.reshape(tf.matmul(
        tf.reshape(tf.one_hot(tf.argmax(output_id_classes, -1), NUM_CLASSES), [-1, NUM_CLASSES]), LABEL_COLOR),
        [-1, IMG_HEIGHT, IMG_WIDTH, 3])
    return output_batch

input_tensor = tf.placeholder(dtype=tf.float32, shape=(1, IMG_HEIGHT, IMG_WIDTH, 3))

predict_tensor = Bisenet(input_tensor, NUM_CLASSES, mode="inference")
predict_image = one_hot_tensors_to_color_images(predict_tensor)


saver = tf.train.Saver()

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
sess.run(tf.global_variables_initializer())
sess.run(tf.local_variables_initializer())
saver.restore(sess, "./model/model.ckpt")

list_images = os.listdir(IMAGE_PATH)
for name in list_images:
    image = cv2.imread(IMAGE_PATH+name)
    image = cv2.resize(image, (IMG_HEIGHT, IMG_WIDTH))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = image / 255.
    image = np.expand_dims(image, axis=0)
    t = time.time()
    predict = sess.run(predict_image, feed_dict={input_tensor: image})
    print(time.time() - t)
    cv2.imwrite(PREDICT_PATH+name, predict[0])
    # cv2.imshow("hjhjhj", image[0])
    # cv2.waitKey()