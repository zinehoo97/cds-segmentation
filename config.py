import numpy as np


NUM_CLASSES = 5
DATASET_SIZE = 1000
BATCH_SIZE = 16
IMAGE_SIZE = 256
THREADS = 8

EPOCHS = 200

LEARNING_RATE = 1e-4
MOMENTUM = 0.9

LABEL_COLOR = np.array([(0, 0, 0), (255, 215, 50), (85, 155, 45), (170, 35, 170), (35, 70, 170)], dtype=np.float32)

EVENT_DIR = "log"
SAVE_MODEL_DIR = "model"
INPUT_DIR = "DataId/RGB"
LABEL_DIR = "DataId/labels"
